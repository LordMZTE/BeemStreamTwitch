// ==UserScript==
// @name         BeemStreamTwitch
// @version      0.1
// @description  Adds a twitch link to BeemStream entries
// @author       LordMZTE
// @include      http*://beemstream.com/
// @include      http*://beemstream.com/browse/*
// ==/UserScript==
