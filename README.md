# NOTE
This repository is archived. since its functionality has been added to BeemStream.
# BeemStreamTwitch
This adds buttons that link directly to a streamer's twitch page under entries on [BeemStream](https://beemstream.com)

# Building
- Make sure [Haxe](https://haxe.org) is installed
- Run `haxe build.hxml`
- Copy the script in `bin/` into a new [Tampermonkey](https://www.tampermonkey.net) script
