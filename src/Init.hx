import haxe.macro.Compiler;
import haxe.macro.Context;
import sys.io.File;

macro function init() {
    var oldHeader = Context.definedValue("source-header");
    var header = Std.string(File.read("assets/header.txt").readAll());
    Compiler.define("source-header", '$header\n//$oldHeader');
    return macro {};
}
