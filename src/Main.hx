import js.Promise;
import js.html.MutationObserver;
import js.html.DOMElement;
import js.Browser.document;
import js.Browser.window;
import js.Browser.console;

using StringTools;
using Lambda;

var grid:Null<DOMElement>;

function main() {
    if (!discoverGrid()) {
        window.setTimeout(main, 20);
        return;
    }
    addButtons();

    var gridObserver = new MutationObserver((_, _) -> addButtons());
    gridObserver.observe(grid, {childList: true});

    var bodyObserver = new MutationObserver((_, _) -> discoverAndAddButtons());
    bodyObserver.observe(document.body, {childList: true, subtree: true});
}

function discoverAndAddButtons() {
    if (!discoverGrid()) {
        window.setTimeout(discoverAndAddButtons, 20);
        return;
    }
    addButtons();
}

/**
    Tries discovering the grid of streams
    returns true if a grid was found
**/
function discoverGrid():Bool {
    if (grid != null && document.body.contains(grid))
        return true;

    var grids = document.getElementsByClassName("grid gap-x-6");

    if (grids == null || grids.length == 0 || grids.item(0) == null) {
        return false;
    }
    grid = grids.item(0);
    return true;
}

function addButtons() {
    console.log("[BeemStreamTwitch] adding buttons to", grid);
    function shouldAdd(el:DOMElement):Bool {
        for (c in el.children)
            if (c.attributes.getNamedItem("twitchButton") != null)
                return false;
        return true;
    }

    for (element in grid.children) {
        var element = cast(element.firstChild.firstChild.childNodes[2].firstChild.firstChild, DOMElement);

        if (!shouldAdd(element))
            continue;

        var streamerName = element.innerText.trim();

        var button = document.createAnchorElement();
        button.setAttribute("href", 'https://twitch.tv/$streamerName');
        // marker
        button.setAttribute("twitchButton", "");

        var img = document.createImageElement();
        img.setAttribute("src", "https://twitch.tv/favicon.ico");

        button.appendChild(img);

        element.appendChild(document.createBRElement());
        element.appendChild(button);
    }
}
